<!doctype html>
<html lang="ru">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport"
    content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">


<link rel="stylesheet" href="{{ asset('css/app.css') }}">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
      integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p"
      crossorigin="anonymous"/>
<link rel="stylesheet" href="{{ asset('css/site.css') }}">
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/site.js') }}"></script>

{{-- <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous"> --}}

<title>Магазин</title>

  </head>
  <body>


    <div class="container">

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-4">
            <!-- Бренд и кнопка «Гамбургер» -->
            <a class="navbar-brand" href="/">Магазин</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse"
            data-target="#navbar-example" aria-controls="navbar-example"
            aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
        <!-- Основная часть меню (может содержать ссылки, формы и прочее) -->
        <div class="collapse navbar-collapse" id="navbar-example">
                <!-- Этот блок расположен слева -->
                <ul class="navbar-nav mr-auto ">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Каталог</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Доставка</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Контакты</a>
                    </li>
                </ul>
                <!-- Этот блок расположен справа -->
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" name="query"
                    placeholder="Поиск по каталогу" aria-label="Search">
             <button class="btn btn-outline-light my-2 my-sm-0"
                     type="submit">Поиск</button>
         </form>
            </div>
        </nav>

        <div class="row">
            <div class="col-md-3">
                <h4>Разделы каталога</h4>
                <p>Здесь будут корневые разделы</p>
                <h4>Популярные бренды</h4>
                <p>Здесь будут популярные бренды</p>
            </div>
            <div class="col-md-9">
                @yield('content')
            </div>
        </div>
    </div>
  </body>
</html>
